package com.example.cache

import com.example.data.models.*
import com.example.data.repository.MovieCache
import javax.inject.Inject

class MovieCacheImpl @Inject constructor() : MovieCache {
    override suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity {
        TODO("Not yet implemented")
    }

    override suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity {
        TODO("Not yet implemented")
    }

    override suspend fun saveMovies(movies: List<MovieEntity>) {
        TODO("Not yet implemented")
    }

    override suspend fun isCached(): Boolean {
        TODO("Not yet implemented")
    }
}