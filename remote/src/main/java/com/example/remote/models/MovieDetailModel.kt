package com.example.remote.models

import com.google.gson.annotations.SerializedName

data class MovieDetailModel(
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String? = null,
    @SerializedName("belongs_to_collection")
    val belongsToCollection: BelongsToCollectionModel? = null,
    val budget: Long,
    val genres: List<GenreModel>,
    val homepage: String,
    val id: Long,
    @SerializedName("imdb_id")
    val imdbID: String,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String? = null,
    @SerializedName("production_companies")
    val productionCompanies: List<ProductionCompanyModel>,
    @SerializedName("production_countries")
    val productionCountries: List<ProductionCountryModel>,
    @SerializedName("release_date")
    val releaseDate: String,
    val revenue: Long,
    val runtime: Long,
    @SerializedName("spoken_languages")
    val spokenLanguages: List<SpokenLanguageModel>,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Long
)


data class BelongsToCollectionModel(
    val id: Long,
    val name: String? = null,
    @SerializedName("poster_path")
    val posterPath: String? = null,
    @SerializedName("backdrop_path")
    val backdropPath: String? = null
)


data class GenreModel(
    val id: Long,
    val name: String
)

data class ProductionCompanyModel(
    val id: Long,
    @SerializedName("logo_path")
    val logoPath: String? = null,
    val name: String,
    @SerializedName("origin_country")
    val originCountry: String
)

data class ProductionCountryModel(
    @SerializedName("iso_3166_1")
    val iso3166_1: String,
    val name: String
)

data class SpokenLanguageModel(
    @SerializedName("english_name")
    val englishName: String,
    @SerializedName("iso_639_1")
    val iso639_1: String,
    val name: String
)

