package com.example.remote.api

import com.example.remote.models.MovieDetailModel
import com.example.remote.models.MovieResponseModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {


    @GET("movie/popular?")
    suspend fun getMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): MovieResponseModel

    @GET("movie/{movie_id}?")
    suspend fun getMovieById(@Path("movie_id") id: Long, @Query("api_key") apiKey: String): MovieDetailModel
}