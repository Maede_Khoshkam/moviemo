package com.example.remote

import com.example.data.models.MovieDetailEntity
import com.example.data.models.MovieListParamsEntity
import com.example.data.models.MovieParamsEntity
import com.example.data.models.MovieResponseEntity
import com.example.data.repository.MovieRemote
import com.example.remote.api.MovieService
import com.example.remote.mapper.mapFromResponse
import javax.inject.Inject


class MovieRemoteImpl @Inject constructor(private val movieService: MovieService) : MovieRemote {

    override suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity {
        return movieService.getMovies(params.apiKey, params.page).mapFromResponse()
    }

    override suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity {
        return movieService.getMovieById(params.movieId, params.apiKey).mapFromResponse()
    }
}


