package com.example.remote.mapper

import com.example.data.models.*
import com.example.remote.models.*

fun MovieDetailModel.mapFromResponse(): MovieDetailEntity {
    return MovieDetailEntity(
        adult,
        backdropPath,
        belongsToCollection?.mapFromResponse(),
        budget,
        genres.mapFromResponse(),
        homepage,
        id,
        imdbID,
        originalLanguage,
        originalTitle,
        overview,
        popularity,
        posterPath,
        productionCompanies.mapFromResponse(),
        productionCountries.mapFromResponse(),
        releaseDate,
        revenue,
        runtime,
        spokenLanguages.mapFromResponse(),
        status,
        tagline,
        title,
        video,
        voteAverage,
        voteCount
    )
}

private fun BelongsToCollectionModel.mapFromResponse(): BelongsToCollectionEntity {
    return BelongsToCollectionEntity(name, posterPath, backdropPath)
}

@JvmName("mapFromResponseSpokenLanguageModel")
private fun List<SpokenLanguageModel>.mapFromResponse(): List<SpokenLanguageEntity> {
    return map {
        val spokenLanguageEntity = SpokenLanguageEntity(
            it.englishName,
            it.iso639_1,
            it.name
        )
        spokenLanguageEntity
    }
}

@JvmName("mapFromResponseProductionCountryModel")
private fun List<ProductionCountryModel>.mapFromResponse(): List<ProductionCountryEntity> {
    return map {
        val productionCountryEntity = ProductionCountryEntity(
            it.iso3166_1,
            it.name
        )
        productionCountryEntity
    }
}

@JvmName("mapFromResponseProductionCompanyModel")
private fun List<ProductionCompanyModel>.mapFromResponse(): List<ProductionCompanyEntity> {
    return map {
        val productionCompanyEntity = ProductionCompanyEntity(
            it.id,
            it.logoPath,
            it.name,
            it.originCountry
        )
        productionCompanyEntity
    }
}

@JvmName("mapFromResponseGenreModel")
private fun List<GenreModel>.mapFromResponse(): List<GenreEntity> {
    return map {
        val genreEntity = GenreEntity(
            it.id,
            it.name
        )
        genreEntity
    }
}


fun MovieResponseModel.mapFromResponse(): MovieResponseEntity {
    return MovieResponseEntity(
        page,
        movies.mapFromResponse(),
        totalPages,
        totalMovies
    )
}

private fun List<MovieModel>.mapFromResponse(): List<MovieEntity> {
    return map {
        val movieEntity = MovieEntity(
            it.adult,
            it.backdropPath,
            it.genreIDS,
            it.id,
            it.originalLanguage,
            it.originalTitle,
            it.overview,
            it.popularity,
            it.posterPath,
            it.releaseDate,
            it.title,
            it.video,
            it.voteAverage,
            it.voteCount
        )
        movieEntity
    }
}
