package com.example.data

import com.example.data.mapper.mapFromEntity
import com.example.data.mapper.mapToEntity
import com.example.data.models.MovieEntity
import com.example.data.source.MovieDataSourceFactory
import com.example.domain.models.*
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(private val dataSourceFactory: MovieDataSourceFactory) :
    MovieRepository {

    override suspend fun getMovies(params: MovieListParams): Flow<MovieResponse> = flow {
//        val isCached = dataSourceFactory.getCacheDataSource().isCached()
        val movieList: MovieResponse =
            dataSourceFactory.getDataStore(false).getMovies(params.mapToEntity()).mapFromEntity()
        emit(movieList)
    }

    override suspend fun getMovie(params: MovieParams): Flow<MovieDetail> = flow {
        val movieDetail: MovieDetail =
            dataSourceFactory.getRemoteDataSource().getMovie(params.mapToEntity()).mapFromEntity()
        emit(movieDetail)
    }

    override suspend fun saveMovies(movies: List<Movie>) {
        val movieEntityList: List<MovieEntity> = movies.mapToEntity()
        dataSourceFactory.getCacheDataSource().saveMovies(movieEntityList)
    }
}







