package com.example.data.repository

import com.example.data.models.*


interface MovieCache {

    suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity
    suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity
    suspend fun saveMovies(movies: List<MovieEntity>)
    suspend fun isCached(): Boolean

}