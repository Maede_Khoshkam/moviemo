package com.example.data.repository

import com.example.data.models.MovieDetailEntity
import com.example.data.models.MovieListParamsEntity
import com.example.data.models.MovieParamsEntity
import com.example.data.models.MovieResponseEntity

interface MovieRemote {

    suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity
    suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity
}