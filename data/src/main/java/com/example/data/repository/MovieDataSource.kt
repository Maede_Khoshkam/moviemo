package com.example.data.repository

import com.example.data.models.*

interface MovieDataSource {
    // Remote and cache
    suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity
    suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity

    // Cache
    suspend fun saveMovies(movies: List<MovieEntity>)
    suspend fun isCached(): Boolean
}