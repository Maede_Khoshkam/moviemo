package com.example.data.source

import com.example.data.models.*
import com.example.data.repository.MovieDataSource
import com.example.data.repository.MovieRemote
import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(private val movieRemote: MovieRemote) :
    MovieDataSource {

    override suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity {
        return movieRemote.getMovies(params)
    }

    override suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity {
        return movieRemote.getMovie(params)
    }

    override suspend fun saveMovies(movies: List<MovieEntity>) {
        throw UnsupportedOperationException("Save character is not supported for RemoteDataSource.")
    }

    override suspend fun isCached(): Boolean {
        throw UnsupportedOperationException("Cache is not supported for RemoteDataSource.")
    }
}