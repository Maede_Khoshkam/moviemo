package com.example.data.source

import com.example.data.models.*
import com.example.data.repository.MovieCache
import com.example.data.repository.MovieDataSource
import javax.inject.Inject

class MovieCacheDataSource @Inject constructor(private val movieCache: MovieCache) :
    MovieDataSource {

    override suspend fun getMovies(params: MovieListParamsEntity): MovieResponseEntity {
        return movieCache.getMovies(params)
    }

    override suspend fun getMovie(params: MovieParamsEntity): MovieDetailEntity {
        return movieCache.getMovie(params)
    }

    override suspend fun saveMovies(movies: List<MovieEntity>) {
        movieCache.saveMovies(movies)
    }

    override suspend fun isCached(): Boolean {
        return movieCache.isCached()
    }


}