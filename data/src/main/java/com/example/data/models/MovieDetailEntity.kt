package com.example.data.models


data class MovieDetailEntity(
    val adult: Boolean,
    val backdropPath: String?,
    val belongsToCollection: BelongsToCollectionEntity?,
    val budget: Long,
    val genres: List<GenreEntity>,
    val homepage: String,
    val id: Long,
    val imdbID: String,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String?,
    val productionCompanies: List<ProductionCompanyEntity>,
    val productionCountries: List<ProductionCountryEntity>,
    val releaseDate: String,
    val revenue: Long,
    val runtime: Long,
    val spokenLanguages: List<SpokenLanguageEntity>,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Long
)


data class BelongsToCollectionEntity(
    val name: String?,
    val posterPath: String?,
    val backdropPath: String?
)


data class GenreEntity(
    val id: Long,
    val name: String
)

data class ProductionCompanyEntity(
    val id: Long,
    val logoPath: String?,
    val name: String,
    val originCountry: String
)

data class ProductionCountryEntity(
    val iso3166_1: String,
    val name: String
)

data class SpokenLanguageEntity(
    val englishName: String,
    val iso639_1: String,
    val name: String
)

