package com.example.data.models

data class MovieListParamsEntity(var apiKey: String, var page: Int)