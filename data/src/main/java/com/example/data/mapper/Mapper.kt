package com.example.data.mapper

import com.example.data.models.*
import com.example.domain.models.*


fun MovieResponseEntity.mapFromEntity(): MovieResponse {
    return MovieResponse(
        page,
        movies.mapFromEntity(),
        totalPages,
        totalMovies
    )
}

fun List<Movie>.mapToEntity(): List<MovieEntity> {
    return map {
        val movieEntity = MovieEntity(
            it.adult,
            it.backdropPath ?: "",
            it.genreIDS,
            it.id,
            it.originalLanguage,
            it.originalTitle,
            it.overview,
            it.popularity,
            it.posterPath,
            it.releaseDate,
            it.title,
            it.video,
            it.voteAverage,
            it.voteCount
        )
        movieEntity
    }
}

fun MovieParams.mapToEntity(): MovieParamsEntity {
    return MovieParamsEntity(apiKey, movieId)
}

fun MovieListParams.mapToEntity(): MovieListParamsEntity {
    return MovieListParamsEntity(apiKey, page)
}


private fun List<MovieEntity>.mapFromEntity(): List<Movie> {
    return map {
        val movie = Movie(
            it.adult,
            it.backdropPath,
            it.genreIDS,
            it.id,
            it.originalLanguage,
            it.originalTitle,
            it.overview,
            it.popularity,
            it.posterPath,
            it.releaseDate,
            it.title,
            it.video,
            it.voteAverage,
            it.voteCount
        )
        movie
    }
}

fun MovieDetailEntity.mapFromEntity(): MovieDetail {
    return MovieDetail(
        adult,
        backdropPath,
        belongsToCollection?.mapFromEntity(),
        budget,
        genres.mapFromEntity(),
        homepage,
        id,
        imdbID,
        originalLanguage,
        originalTitle,
        overview,
        popularity,
        posterPath,
        productionCompanies.mapFromEntity(),
        productionCountries.mapFromEntity(),
        releaseDate,
        revenue,
        runtime,
        spokenLanguages.mapFromEntity(),
        status,
        tagline,
        title,
        video,
        voteAverage,
        voteCount
    )


}

@JvmName("mapFromEntitySpokenLanguageEntity")
private fun List<SpokenLanguageEntity>.mapFromEntity(): List<SpokenLanguage> {
    return map {
        val spokenLanguage = SpokenLanguage(
            it.englishName,
            it.iso639_1,
            it.name
        )
        spokenLanguage
    }
}

@JvmName("mapFromEntityProductionCountryEntity")
private fun List<ProductionCountryEntity>.mapFromEntity(): List<ProductionCountry> {
    return map {
        val productionCountry = ProductionCountry(
            it.iso3166_1,
            it.name
        )
        productionCountry
    }
}

@JvmName("mapFromEntityProductionCompanyEntity")
private fun List<ProductionCompanyEntity>.mapFromEntity(): List<ProductionCompany> {
    return map {
        val productionCompany = ProductionCompany(
            it.id,
            it.logoPath,
            it.name,
            it.originCountry

            )
        productionCompany
    }
}

@JvmName("mapFromEntityGenreEntity")
private fun List<GenreEntity>.mapFromEntity(): List<Genre> {
    return map {
        val genre = Genre(
            it.id,
            it.name
        )
        genre
    }
}

private fun BelongsToCollectionEntity.mapFromEntity(): BelongsToCollection {
    return BelongsToCollection(
        name,
        posterPath,
        backdropPath
    )
}
