package com.example.domain.interactor

import com.example.domain.models.MovieListParams
import com.example.domain.models.MovieResponse
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(private val movieRepository: MovieRepository) :
    BaseUseCase<MovieListParams, Flow<MovieResponse>> {
    override suspend fun invoke(params: MovieListParams): Flow<MovieResponse> =
        movieRepository.getMovies(params)

}