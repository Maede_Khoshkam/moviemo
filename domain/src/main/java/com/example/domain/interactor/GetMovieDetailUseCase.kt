package com.example.domain.interactor

import com.example.domain.models.MovieDetail
import com.example.domain.models.MovieParams
import com.example.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieDetailUseCase @Inject constructor(private val movieRepository: MovieRepository) :
    BaseUseCase<MovieParams, Flow<MovieDetail>> {
    override suspend fun invoke(params: MovieParams): Flow<MovieDetail> =
        movieRepository.getMovie(params)
}