package com.example.domain.repository

import com.example.domain.models.*
import kotlinx.coroutines.flow.Flow

interface MovieRepository {

    // Remote and cache
    suspend fun getMovies(params: MovieListParams): Flow<MovieResponse>
    suspend fun getMovie(params: MovieParams): Flow<MovieDetail>

    // Cache
    suspend fun saveMovies(movies: List<Movie>)

}