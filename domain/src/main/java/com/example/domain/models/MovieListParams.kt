package com.example.domain.models

data class MovieListParams(var apiKey: String, var page: Int) {
}