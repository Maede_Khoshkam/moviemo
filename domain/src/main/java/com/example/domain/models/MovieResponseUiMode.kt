package com.example.domain.models

sealed class MovieResponseUiMode : UiAwareModel() {

    object Loading : MovieResponseUiMode()
    data class Success(val data: MovieResponse) : MovieResponseUiMode()
    data class Error(var error: String) : MovieResponseUiMode()
}