package com.example.domain.models

sealed class MovieDetailUiModel : UiAwareModel() {

    object Loading : MovieDetailUiModel()
    data class Success(val data: MovieDetail) : MovieDetailUiModel()
    data class Error(var error: String) : MovieDetailUiModel()
}