package com.example.domain.models

data class MovieParams(var apiKey: String, var movieId: Long) {
}