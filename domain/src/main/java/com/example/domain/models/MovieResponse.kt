package com.example.domain.models

data class MovieResponse(
    val page: Long,
    val movies: List<Movie>,
    val totalPages: Long,
    val totalMovies: Long
)


data class Movie(
    val adult: Boolean,

    var backdropPath: String? = null,

    val genreIDS: List<Long>,

    val id: Long,

    val originalLanguage: String,

    val originalTitle: String,

    val overview: String,

    val popularity: Double,

    val posterPath: String?,

    val releaseDate: String? = null,

    val title: String,

    val video: Boolean,

    val voteAverage: Double,

    val voteCount: Long
)

