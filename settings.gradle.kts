rootProject.name = "MovieMo"
include(":app")
include(":cache")
include(":presentation")
include(":remote")
include(":domain")
include(":data")
