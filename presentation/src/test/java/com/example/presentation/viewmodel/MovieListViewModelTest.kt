package com.example.presentation.viewmodel

import androidx.lifecycle.Observer
import com.example.domain.interactor.GetMovieListUseCase
import com.example.domain.models.MovieListParams
import com.example.domain.models.MovieResponseUiMode
import com.example.presentation.fakes.FakePresentationData
import com.example.presentation.utils.PresentationBaseTest
import com.example.presentation.viewModel.MovieListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.IOException

@ExperimentalCoroutinesApi
class MovieListViewModelTest : PresentationBaseTest() {

    @Mock
    private lateinit var getMovieListUseCase: GetMovieListUseCase

    @Mock
    private lateinit var movieListViewModel: MovieListViewModel


    @Mock
    private lateinit var observer: Observer<MovieResponseUiMode>

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        movieListViewModel = MovieListViewModel(dispatcher, getMovieListUseCase)

        movieListViewModel.movieList.observeForever(observer)
    }


    @Test
    fun `get movie should return movie response from use-case`() {
        dispatcher.test.runBlockingTest {
            // Arrange (Given)
            val movieResponse = FakePresentationData.getMovieResponse(10)
            val params = MovieListParams("", 1)
            whenever(getMovieListUseCase(params)).thenReturn(flowOf(movieResponse))
            // Act (When)
            movieListViewModel.getMovies(params)
            // Assert (Then)
            verify(observer).onChanged(MovieResponseUiMode.Loading)
            verify(observer).onChanged(MovieResponseUiMode.Success(movieResponse))
        }
    }

    @Test
    fun `get movie should return empty movie list from use-case`() {
        dispatcher.test.runBlockingTest {
            // Arrange (Given)
            val movieResponse = FakePresentationData.getMovieResponse(0)
            val params = MovieListParams("", 1)
            whenever(getMovieListUseCase(params)).thenReturn(flowOf(movieResponse))
            // Act (When)
            movieListViewModel.getMovies(params)
            // Assert (Then)
            verify(observer).onChanged(MovieResponseUiMode.Loading)
            verify(observer).onChanged(MovieResponseUiMode.Success(movieResponse))
        }
    }

    @Test
    fun `get movie should return error from use-case`() =
        dispatcher.test.runBlockingTest {
            // Arrange (Given)
            val errorMessage = "Internal server error"
            val params = MovieListParams("", 1)
            whenever(getMovieListUseCase(params)).thenAnswer {
                throw IOException(errorMessage)
            }

            // Act (When)
            movieListViewModel.getMovies(params)

            // Assert (Then)
            verify(observer).onChanged(MovieResponseUiMode.Loading)
            verify(observer).onChanged(MovieResponseUiMode.Error(errorMessage))
        }


    @After
    fun tearDown() {
        movieListViewModel.onCleared()
    }

}