package com.example.presentation.viewmodel

import androidx.lifecycle.Observer
import com.example.domain.interactor.GetMovieDetailUseCase
import com.example.domain.models.MovieDetailUiModel
import com.example.domain.models.MovieParams
import com.example.presentation.fakes.FakePresentationData
import com.example.presentation.utils.PresentationBaseTest
import com.example.presentation.viewModel.MovieDetailViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.IOException


@ExperimentalCoroutinesApi
class MovieDetailViewModelTest : PresentationBaseTest() {


    @Mock
    private lateinit var movieDetailUseCase: GetMovieDetailUseCase

    @Mock
    private lateinit var observer: Observer<MovieDetailUiModel>

    private lateinit var movieDetailViewMode: MovieDetailViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        movieDetailViewMode = MovieDetailViewModel(dispatcher, movieDetailUseCase)
        movieDetailViewMode.movieDetailResponse.observeForever(observer)
    }

    @Test
    fun `get movie detail with movie-id should return movie complete detail from use-case`() =
        dispatcher.test.runBlockingTest {
            //Given
            val params = MovieParams("", 1L)
            val movieDetail = FakePresentationData.getMovieDetail()
            whenever(movieDetailUseCase(params)).thenReturn(flowOf(movieDetail))

            //when
            movieDetailViewMode.getMovieDetail(params)

            //then
            verify(observer).onChanged(MovieDetailUiModel.Loading)
            verify(observer).onChanged(MovieDetailUiModel.Success(movieDetail))
        }

    @Test
    fun `get movie detail with movie-id should return error from use-case`() =
        dispatcher.test.runBlockingTest {
            //Given
            val params = MovieParams("", 1L)
            val errorMessage = "Internal server error"
            whenever(movieDetailUseCase(params)).thenAnswer {
                throw IOException(errorMessage)
            }
            //when
            movieDetailViewMode.getMovieDetail(params)

            //then
            verify(observer).onChanged(MovieDetailUiModel.Loading)
            verify(observer).onChanged(MovieDetailUiModel.Error(errorMessage))
        }


}