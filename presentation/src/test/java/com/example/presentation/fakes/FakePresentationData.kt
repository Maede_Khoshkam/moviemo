package com.example.presentation.fakes

import com.example.domain.models.*
import com.example.presentation.fakes.FakeValueFactory.randomBoolean
import com.example.presentation.fakes.FakeValueFactory.randomDouble
import com.example.presentation.fakes.FakeValueFactory.randomInt
import com.example.presentation.fakes.FakeValueFactory.randomLong
import com.example.presentation.fakes.FakeValueFactory.randomString


object FakePresentationData {


    fun getMovieResponse(size: Int): MovieResponse {
        return MovieResponse(randomLong(), getMovies(size), randomLong(), randomLong())
    }

    fun getMovies(
        size: Int
    ): List<Movie> {
        val movies = mutableListOf<Movie>()
        repeat(size) {
            movies.add(createMovie())
        }
        return movies
    }

    fun getMovieDetail(): MovieDetail {
        return MovieDetail(
            randomBoolean(),
            randomString(),
            createBelongToCollection(),
            randomLong(),
            createGenreList(),
            randomString(),
            randomLong(),
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            randomDouble(),
            randomString(),
            createProductionCompanies(),
            createCountries(),
            randomString(),
            randomLong(),
            randomLong(),
            createSpokenLanguages(),
            randomString(),
            randomString(),
            randomString(),
            randomBoolean(),
            randomDouble(),
            randomLong()
        )
    }

    private fun createSpokenLanguages(): List<SpokenLanguage> {
        val size = randomInt(10)
        val list = mutableListOf<SpokenLanguage>()
        repeat(size) {
            list.add(SpokenLanguage(randomString(), randomString(), randomString()))
        }

        return list
    }

    private fun createCountries(): List<ProductionCountry> {
        val size = randomInt(10)
        val list = mutableListOf<ProductionCountry>()
        repeat(size) {
            list.add(ProductionCountry(randomString(), randomString()))
        }
        return list
    }

    private fun createProductionCompanies(): List<ProductionCompany> {
        val size = randomInt(10)
        val list = mutableListOf<ProductionCompany>()
        repeat(size) {
            list.add(
                ProductionCompany(
                    randomLong(),
                    randomString(),
                    randomString(),
                    randomString()
                )
            )
        }
        return list
    }

    private fun createGenreList(): List<Genre> {
        val size = randomInt(10)
        val list = mutableListOf<Genre>()
        repeat(size) {
            list.add(
                Genre(randomLong(), randomString())
            )
        }
        return list
    }

    private fun createBelongToCollection(): BelongsToCollection {
        return BelongsToCollection(randomString(), randomString(), randomString())
    }

    private fun createMovie(): Movie {
        return Movie(
            adult = randomBoolean(),
            backdropPath = randomString(),
            genreIDS = createGenreIDS(randomInt(10)),
            id = randomLong(),
            originalLanguage = randomString(),
            originalTitle = randomString(),
            overview = randomString(),
            popularity = randomDouble(),
            posterPath = randomString(),
            releaseDate = randomString(),
            title = randomString(),
            video = randomBoolean(),
            voteAverage = randomDouble(),
            voteCount = randomLong()
        )
    }

    private fun createGenreIDS(size: Int): List<Long> {
        val genreIDS = mutableListOf<Long>()
        repeat(size) {
            genreIDS.add(randomLong())
        }
        return genreIDS
    }

}