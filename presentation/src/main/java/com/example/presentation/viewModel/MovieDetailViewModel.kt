package com.example.presentation.viewModel

import androidx.lifecycle.LiveData
import com.example.domain.interactor.GetMovieDetailUseCase
import com.example.domain.models.MovieDetailUiModel
import com.example.domain.models.MovieParams
import com.example.presentation.utils.CoroutineContextProvider
import com.example.presentation.utils.UiAwareLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val coroutineContextProvider: CoroutineContextProvider,
    private val getMovieDetailUseCase: GetMovieDetailUseCase
) : BaseViewModel(coroutineContextProvider) {


    private val _movieDetailResponse = UiAwareLiveData<MovieDetailUiModel>()
    val movieDetailResponse: LiveData<MovieDetailUiModel> = _movieDetailResponse

    override val coroutineExceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            _movieDetailResponse.postValue(MovieDetailUiModel.Error(throwable.message ?: "Error"))
        }


    fun getMovieDetail(params: MovieParams) {
        _movieDetailResponse.postValue(MovieDetailUiModel.Loading)
        launchCoroutineIO {
            loadMovieDetail(params)
        }
    }

    private suspend fun loadMovieDetail(params: MovieParams) {
        getMovieDetailUseCase(params).collect {
            _movieDetailResponse.postValue(MovieDetailUiModel.Success(it))
        }
    }

}