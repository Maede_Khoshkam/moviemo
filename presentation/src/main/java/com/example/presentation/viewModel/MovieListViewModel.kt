package com.example.presentation.viewModel

import androidx.lifecycle.LiveData
import com.example.domain.interactor.GetMovieListUseCase
import com.example.domain.models.MovieListParams
import com.example.domain.models.MovieResponseUiMode
import com.example.presentation.utils.CoroutineContextProvider
import com.example.presentation.utils.UiAwareLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.collect
import javax.inject.Inject
import kotlin.math.ceil


@HiltViewModel
class MovieListViewModel @Inject constructor(
    private val coroutineContextProvider: CoroutineContextProvider,
    private val getMovieListUseCase: GetMovieListUseCase
) : BaseViewModel(coroutineContextProvider) {


    private val _movieList = UiAwareLiveData<MovieResponseUiMode>()
    val movieList: LiveData<MovieResponseUiMode> = _movieList
    private var totalMovies: Long = 20
    private var mPageSize: Int = 20

    override val coroutineExceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            _movieList.postValue(MovieResponseUiMode.Error(throwable.message ?: "Error"))
        }

    fun getMovies(params: MovieListParams) {
        _movieList.postValue(MovieResponseUiMode.Loading)
        launchCoroutineIO {
            loadMovies(params)
        }
    }

    private suspend fun loadMovies(params: MovieListParams) {
        getMovieListUseCase(params).collect {
            totalMovies = it.totalMovies
            _movieList.postValue(MovieResponseUiMode.Success(it))
        }
    }


    fun getNextPage(itemCount: Int): Int {
        return getCurrentPage(itemCount) + 1
    }

    private fun getCurrentPage(itemCount: Int): Int {
        return ceil((itemCount / mPageSize).toDouble()).toInt()
    }

    fun isEndOfList(itemCount: Int): Boolean {
        return totalMovies <= itemCount
    }


}