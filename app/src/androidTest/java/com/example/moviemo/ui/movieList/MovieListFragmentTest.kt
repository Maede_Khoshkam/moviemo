package com.example.moviemo.ui.movieList

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.moviemo.R
import com.example.moviemo.ui.MovieActivity
import com.example.moviemo.ui.movieList.MovieAdapter.*
import com.example.moviemo.ui.movieList.fakes.FakeData
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class MovieListFragmentTest {


    @get:Rule
    val activity = ActivityScenarioRule(MovieActivity::class.java)
    val LIST_ITEM_IN_TEST = 0
    val MOVIE_IN_TEST = FakeData.getMovie()

    /**
     * Recyclerview comes into view
     */
    @Test
    fun test_isMovieListFragmentVisible_onAppLaunch() {
        onView(withId(R.id.recyclerViewList)).check(matches(isDisplayed()))
    }

    /**
     * Select list item , nav to detail fragment
     * Correct movie is in view ?
     */
    @Test
    fun test_selectMovieListItem_isMovieDetailFragmentVisible() {

        onView(withId(R.id.recyclerViewList)).perform(
            actionOnItemAtPosition<MovieViewHolder>(
                LIST_ITEM_IN_TEST,
                click()
            )
        )

        onView(withId(R.id.movie_title)).check(matches(withText(MOVIE_IN_TEST.title)))
    }

    /**
     * Select list item , nav to detail fragment
     * press back
     */

    @Test
    fun test_backNavigation_toMovieListFragment() {
        onView(withId(R.id.recyclerViewList)).perform(
            actionOnItemAtPosition<MovieViewHolder>(
                LIST_ITEM_IN_TEST,
                click()
            )
        )
        onView(withId(R.id.movie_title)).check(matches(withText(MOVIE_IN_TEST.title)))
        pressBack()
        onView(withId(R.id.recyclerViewList)).check(matches(isDisplayed()))

    }

}