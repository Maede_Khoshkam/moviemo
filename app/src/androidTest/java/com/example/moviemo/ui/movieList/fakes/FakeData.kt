package com.example.moviemo.ui.movieList.fakes

import com.example.domain.models.*


object FakeData {


    fun getMovies(
        size: Int
    ): List<Movie> {
        val movies = mutableListOf<Movie>()
        repeat(size) {
            movies.add(getMovie())
        }
        return movies
    }

    fun getMovie(): Movie {
        return Movie(
            false,
            "/1QW1QHeeW3rZXl6FNKSuqb1kcl8.jpg",
            mutableListOf(),
            476669,
            "en",
            "The King's Man",
            "As a collection of history's worst tyrants and criminal masterminds gather to plot a war to wipe out millions, one man must race against time to stop them.",
            1217.394,
            "/aq4Pwv5Xeuvj6HZKtxyd23e6bE9.jpg",
            "2021-12-22",
            "The King's Man",
            false,
            7.0,
            2364
        )
    }


}