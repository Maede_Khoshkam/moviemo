package com.example.moviemo.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListenerLinear(
    private val mLinearLayoutManager: LinearLayoutManager
) :
    RecyclerView.OnScrollListener() {

    private var currentPage = 1
    private var visibleThreshold = 5
    private var lastVisibleItem = 0
    private var totalItemCount = 0
    private var isLoading: Boolean = false


    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy > 0) {
            totalItemCount = mLinearLayoutManager.itemCount
            lastVisibleItem =
                mLinearLayoutManager.findLastCompletelyVisibleItemPosition()
            if (!isLoading) {
                if (totalItemCount <= lastVisibleItem + visibleThreshold) {
                    isLoading = true
                    currentPage++
                    onLoadMore()
                }
            }
        }
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        onScrollStateChanged(newState)
    }

    abstract fun onLoadMore()
    abstract fun onScrollStateChanged(newState: Int)

    fun newDataReceived(newData: Boolean) {
        isLoading = false
        totalItemCount = 0
        lastVisibleItem = 0
        if (currentPage > 1 && !newData)
            currentPage--
    }

}