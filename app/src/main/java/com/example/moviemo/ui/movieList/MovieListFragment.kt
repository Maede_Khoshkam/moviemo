package com.example.moviemo.ui.movieList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.models.MovieListParams
import com.example.domain.models.MovieResponseUiMode
import com.example.moviemo.base.BaseFragment
import com.example.moviemo.core.Const
import com.example.moviemo.databinding.FragmentMovieListBinding
import com.example.moviemo.extension.gone
import com.example.moviemo.extension.observe
import com.example.moviemo.extension.visible
import com.example.moviemo.utils.EndlessRecyclerOnScrollListenerLinear
import com.example.presentation.viewModel.MovieListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieListFragment : BaseFragment<FragmentMovieListBinding, MovieListViewModel>() {


    @Inject
    lateinit var mAdapter: MovieAdapter
    override val mViewModel: MovieListViewModel by viewModels()
    private lateinit var scrollListener: EndlessRecyclerOnScrollListenerLinear
    private var mPageIndex: Int = 1


    override fun getViewBinding(): FragmentMovieListBinding {
        return FragmentMovieListBinding.inflate(LayoutInflater.from(requireContext()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMovies(mPageIndex)
        observe(mViewModel.movieList, ::onViewStateChange)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
    }

    private fun getMovies(page: Int) {
        val params = MovieListParams(Const.api_key, page)
        mViewModel.getMovies(params)
    }

    private fun setUpRecyclerView() {
        mBinding.recyclerViewList.apply {
            adapter = mAdapter
            val mLinearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = mLinearLayoutManager
            scrollListener = object :
                EndlessRecyclerOnScrollListenerLinear(mLinearLayoutManager) {
                override fun onScrollStateChanged(newState: Int) {
                }

                override fun onLoadMore() {
                    if (!mViewModel.isEndOfList(mAdapter.itemCount)) {
                        mPageIndex = mViewModel.getNextPage(mAdapter.itemCount)
                        getMovies(mPageIndex)
                    }
                }
            }
            addOnScrollListener(scrollListener)
        }

        mAdapter.setItemClickListener { movie ->
            findNavController().navigate(
                MovieListFragmentDirections.actionMovieListFragmentToMovieDetailFragment(
                    movieId = movie.id
                )
            )
        }
    }

    private fun onViewStateChange(event: MovieResponseUiMode) {
        if (event.isRedelivered) return
        when (event) {
            is MovieResponseUiMode.Loading -> {
                mBinding.loading.visible()
            }
            is MovieResponseUiMode.Success -> {
                mBinding.loading.gone()
                scrollListener.newDataReceived(true)
                mAdapter.setData(event.data.movies)
            }
            is MovieResponseUiMode.Error -> {
                mBinding.loading.gone()
                scrollListener.newDataReceived(false)
                handleErrorMessage(event.error)
            }
        }
    }

}

