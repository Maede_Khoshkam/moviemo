package com.example.moviemo.ui.movieList

import android.view.LayoutInflater
import android.view.ViewGroup

import com.bumptech.glide.RequestManager
import com.example.domain.models.Movie
import com.example.moviemo.base.BaseAdapter
import com.example.moviemo.base.BaseViewHolder
import com.example.moviemo.databinding.MovieItemBinding
import com.example.moviemo.extension.makeImageUrl
import javax.inject.Inject

class MovieAdapter @Inject constructor(private val glide: RequestManager) : BaseAdapter<Movie>() {


    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Movie> {
        val binding = MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }


    inner class MovieViewHolder(private val binding: MovieItemBinding) :
        BaseViewHolder<Movie>(binding.root) {
        override fun onBind(item: Movie) {
            binding.apply {
                movieTitle.text = item.title
                movieDescription.text = item.overview
                glide.load(item.backdropPath?.makeImageUrl()).into(movieImage)
                root.setOnClickListener {
                    onItemClickListener?.let { itemClick ->
                        itemClick(item)
                    }
                }
            }
        }

    }
}


