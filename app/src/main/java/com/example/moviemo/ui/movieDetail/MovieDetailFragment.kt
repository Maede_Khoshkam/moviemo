package com.example.moviemo.ui.movieDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.example.domain.models.MovieDetail
import com.example.domain.models.MovieDetailUiModel
import com.example.domain.models.MovieParams
import com.example.moviemo.base.BaseFragment
import com.example.moviemo.core.Const
import com.example.moviemo.databinding.FragmentMovieDetailBinding
import com.example.moviemo.extension.makeImageUrl
import com.example.moviemo.extension.makeImageUrlRectangle
import com.example.moviemo.extension.observe
import com.example.presentation.viewModel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailFragment : BaseFragment<FragmentMovieDetailBinding, MovieDetailViewModel>() {

    @Inject
    lateinit var glide: RequestManager
    private val args: MovieDetailFragmentArgs by navArgs()
    override val mViewModel: MovieDetailViewModel by viewModels()

    override fun getViewBinding(): FragmentMovieDetailBinding =
        FragmentMovieDetailBinding.inflate(LayoutInflater.from(requireContext()))


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val params = MovieParams(Const.api_key, args.movieId)
        mViewModel.getMovieDetail(params)
        observe(mViewModel.movieDetailResponse, ::onViewStateChange)

    }

    private fun onViewStateChange(event: MovieDetailUiModel) {
        if (event.isRedelivered) return
        when (event) {
            is MovieDetailUiModel.Loading -> {
                mBinding.loading.visibility = View.VISIBLE
            }
            is MovieDetailUiModel.Success -> {
                mBinding.loading.visibility = View.GONE
                bindViews(event.data)
            }
            is MovieDetailUiModel.Error -> {
                mBinding.loading.visibility = View.GONE
                handleErrorMessage(event.error)
            }
        }

    }

    private fun bindViews(data: MovieDetail) {
        mBinding.apply {
            title.text = data.title
            overView.text = data.overview
            glide.load(data.backdropPath?.makeImageUrlRectangle()).into(image)
            if (data.productionCompanies.isNotEmpty())
                glide.load(data.productionCompanies[0].logoPath?.makeImageUrl()).into(companyLogo)
        }
    }


}